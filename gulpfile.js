// TEMPLATE GULPFILE

var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var minifyJS = require('gulp-uglify');
var browserSync = require('browser-sync');

gulp.task('sass', function () {
  gulp.src('./src/scss/*.scss')
    .pipe(sass())
    .pipe(cleanCss())
    .pipe(gulp.dest('./public/css/'))
});

gulp.task('minifyJS', function () {
     gulp.src('./src/js/*.js')
    .pipe(minifyJS())
    .pipe(gulp.dest('./public/js/'))
});

gulp.task('browser-sync', function () {
  browserSync.init({
    server: "./public/"
  });
});

gulp.task('gulpwatch', ['sass', 'minifyJS', 'browser-sync'], function () {
  gulp.watch('./src/scss/*.scss', ['sass']).on('change', browserSync.reload);
  gulp.watch('./src/js/*.js', ['minifyJS']).on('change', browserSync.reload);
  gulp.watch('./public/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['gulpwatch']);